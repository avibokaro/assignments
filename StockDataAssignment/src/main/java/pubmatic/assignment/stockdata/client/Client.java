/**
 *
 */
package pubmatic.assignment.stockdata.client;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import pubmatic.assignment.stockdata.model.StockData;
import pubmatic.assignment.stockdata.threads.StockCodeConsumer;
import pubmatic.assignment.stockdata.threads.StockCodeProducer;
import pubmatic.assignment.stockdata.threads.StockDataConsumer;
import pubmatic.assignment.stockdata.util.CSVFileReader;

/**
 * @author avinash
 *
 */
public class Client {
	private static final Logger LOGGER = Logger.getLogger(Client.class);
	public static void main(String[] args) throws IOException {
		LOGGER.info("Starting the program....");
		long startTime = System.currentTimeMillis();
		BlockingQueue<String> stockCodeQueue = new ArrayBlockingQueue<>(1024);
		BlockingQueue<StockData> stockDataQueue = new ArrayBlockingQueue<>(1024);
		StockCodeProducer stockCodeProducer = new StockCodeProducer(stockCodeQueue);
		new Thread(stockCodeProducer).start();
		BufferedWriter bw = null;
		try {
			bw = initFileCreation();
			StockCodeConsumer stockCodeConsumer = new StockCodeConsumer(stockCodeQueue, stockDataQueue);
			StockDataConsumer stockDataConsumer = new StockDataConsumer(stockDataQueue, bw);

			ExecutorService executorService = Executors.newFixedThreadPool(30);
			for (int i = 0; i < CSVFileReader.size; i++) {
				executorService.execute(stockCodeConsumer);
				executorService.execute(stockDataConsumer);
			}
			executorService.shutdown();
			executorService.awaitTermination(60, TimeUnit.SECONDS);
			long endTime = System.currentTimeMillis();
			LOGGER.info("output.csv is generated and execution completed in time=" + (endTime - startTime) / 1000
					+ "seconds");
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			if (bw != null) {
				bw.close();
			}
		}
	}

	private static BufferedWriter initFileCreation() throws IOException {
		File outputCSV = new File("output.csv");
		if (outputCSV.exists()) {
			outputCSV.delete();
			outputCSV.createNewFile();
		}
		String header = "Stock Symbol, Current Price, Year Target Price, Year High, Year Low";
		BufferedWriter bw = new BufferedWriter(new FileWriter(outputCSV));
		bw.write(header);
		bw.write("\n");
		bw.flush();
		return bw;
	}
}
