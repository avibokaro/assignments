/**
 *
 */
package pubmatic.assignment.stockdata.util;

import java.io.BufferedWriter;
import java.io.IOException;

import pubmatic.assignment.stockdata.model.StockData;

/**
 * @author avinash
 *
 */
public final class CSVFileWriter {
	private static final String SEPARATOR = ",";
	public static void writeToCSVFile(StockData stockData, BufferedWriter bw) throws IOException {
		String data = new StringBuilder()
				.append(stockData.getStockSymbol())
				.append(SEPARATOR)
				.append(stockData.getCurrentPrice())
				.append(SEPARATOR)
				.append(stockData.getYearTargetPrice())
				.append(SEPARATOR)
				.append(stockData.getYearHigh())
				.append(SEPARATOR)
				.append(stockData.getYearLow())
				.append("\n")
				.toString();
		bw.write(data);
		bw.flush();
	}
}
