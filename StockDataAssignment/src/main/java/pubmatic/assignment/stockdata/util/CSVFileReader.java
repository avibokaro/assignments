/**
 *
 */
package pubmatic.assignment.stockdata.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;

/**
 * @author avinash
 *
 */
public final class CSVFileReader {
	public static volatile int size = 0;
	public static void readFile(String location, BlockingQueue<String> stockCodeQueue)
			throws IOException, InterruptedException {
		File csvFile = new File(location);
		try
		(BufferedReader br = new BufferedReader(new FileReader(csvFile))){
			String line=null;
			while ((line = br.readLine()) != null) {
				size++;
				stockCodeQueue.put(line);
			}
		}
	}
}
