/**
 *
 */
package pubmatic.assignment.stockdata.model;

/**
 * @author avinash
 *
 */
public class StockData {
	private String stockSymbol;
	private String currentPrice = Constant.DEFAULT_VALUE;
	private String yearTargetPrice = Constant.DEFAULT_VALUE;
	private String yearHigh = Constant.DEFAULT_VALUE;
	private String yearLow = Constant.DEFAULT_VALUE;

	public String getStockSymbol() {
		return stockSymbol;
	}

	public void setStockSymbol(String stockSymbol) {
		this.stockSymbol = stockSymbol;
	}

	public String getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(String currentPrice) {
		if (currentPrice != null && !currentPrice.equalsIgnoreCase(Constant.NULL)) {
			this.currentPrice = currentPrice;
		}
	}

	public String getYearTargetPrice() {
		return yearTargetPrice;
	}

	public void setYearTargetPrice(String yearTargetPrice) {
		if (yearTargetPrice != null && !yearTargetPrice.equalsIgnoreCase(Constant.NULL)) {
			this.yearTargetPrice = yearTargetPrice;
		}
	}

	public String getYearHigh() {
		return yearHigh;
	}

	public void setYearHigh(String yearHigh) {
		if (yearHigh != null && !yearHigh.equalsIgnoreCase(Constant.NULL)) {
			this.yearHigh = yearHigh;
		}
	}

	public String getYearLow() {
		return yearLow;
	}

	public void setYearLow(String yearLow) {
		if (yearLow != null && !yearLow.equalsIgnoreCase(Constant.NULL)) {
			this.yearLow = yearLow;
		}
	}

	@Override
	public String toString() {
		return "StockData [stockSymbol=" + stockSymbol + ", currentPrice=" + currentPrice + ", yearTargetPrice="
				+ yearTargetPrice + ", yearHigh=" + yearHigh + ", yearLow=" + yearLow + "]";
	}

}
