/**
 *
 */
package pubmatic.assignment.stockdata.model;

/**
 * @author avinash
 *
 */
public class Constant {
	public static final String DEFAULT_VALUE = "-1";
	public static final String NULL = "null";
	public static final String API_URL = "https://query.yahooapis.com/v1/public/yql";
	public static final String QUERY = "select * from yahoo.finance.quotes where symbol in ('%s')";
	public static final String ENV = "store://datatables.org/alltableswithkeys";
}
