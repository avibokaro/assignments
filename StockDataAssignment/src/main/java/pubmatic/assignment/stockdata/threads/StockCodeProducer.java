/**
 *
 */
package pubmatic.assignment.stockdata.threads;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;

import pubmatic.assignment.stockdata.util.CSVFileReader;

/**
 * @author avinash
 *
 */
public class StockCodeProducer implements Runnable {

	private BlockingQueue<String> stockCodeQueue;
	public StockCodeProducer(BlockingQueue<String> stockCodeQueue) {
		this.stockCodeQueue = stockCodeQueue;
	}
	@Override
	public void run() {
		try {
			CSVFileReader.readFile("Stocks.txt", stockCodeQueue);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

	}

}
