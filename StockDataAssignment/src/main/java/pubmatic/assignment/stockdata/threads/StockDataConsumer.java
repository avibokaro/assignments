/**
 *
 */
package pubmatic.assignment.stockdata.threads;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;

import pubmatic.assignment.stockdata.model.StockData;
import pubmatic.assignment.stockdata.util.CSVFileWriter;

/**
 * @author avinash
 *
 */
public class StockDataConsumer implements Runnable {
	private BufferedWriter bw = null;
	private BlockingQueue<StockData> stockDataQueue;

	public StockDataConsumer(BlockingQueue<StockData> stockDataQueue, BufferedWriter bw) {
		this.stockDataQueue = stockDataQueue;
		this.bw = bw;
	}
	@Override
	public void run() {
		try {
			CSVFileWriter.writeToCSVFile(stockDataQueue.take(), bw);
		} catch (InterruptedException | IOException e) {
			e.printStackTrace();
		}
	}
}
