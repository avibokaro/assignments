/**
 *
 */
package pubmatic.assignment.stockdata.threads;

import java.util.concurrent.BlockingQueue;

import pubmatic.assignment.stockdata.model.StockData;
import pubmatic.assignment.stockdata.service.StockService;

/**
 * @author avinash
 *
 */
public class StockCodeConsumer implements Runnable {

	private BlockingQueue<String> stockCodeQueue;
	private BlockingQueue<StockData> stockDataQueue;
	private StockService stockService = StockService.INSTANCE;

	public StockCodeConsumer(BlockingQueue<String> stockCodeQueue, BlockingQueue<StockData> stockDataQueue) {
		this.stockCodeQueue = stockCodeQueue;
		this.stockDataQueue = stockDataQueue;
	}
	@Override
	public void run() {
		try {
			stockService.getStockData(stockCodeQueue.take(), stockDataQueue);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
