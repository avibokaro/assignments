/**
 *
 */
package pubmatic.assignment.stockdata.service;

import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

/**
 * @author avinash
 * @param <K>
 * @param <V>
 */
public class InMemoryCache<K, V> implements CacheService<K, V> {
	Cache<K, V> cache = CacheBuilder.newBuilder()
			.maximumSize(1000).expireAfterWrite(1, TimeUnit.MINUTES).build();

	@Override
	public V get(K key) {
		return cache.getIfPresent(key);
	}

	@Override
	public void put(K key, V value) {
		cache.put(key, value);
	}
}
