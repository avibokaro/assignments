/**
 *
 */
package pubmatic.assignment.stockdata.service;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.util.UriComponentsBuilder;

import pubmatic.assignment.stockdata.model.Constant;
import pubmatic.assignment.stockdata.model.StockData;

/**
 * @author avinash
 *
 */
public class StockService {
	public static final StockService INSTANCE = new StockService();
	private CacheService<String, StockData> cacheService = new InMemoryCache<>();
	public StockData getStockData(String stockCode, BlockingQueue<StockData> stockDataQueue)
			throws InterruptedException {
		StockData stockData = checkInCache(stockCode);
		if(stockData==null){
			try {
				HttpResponse response = getAPIResponse(stockCode);
				if (response != null) {
					stockData = new StockData();
					stockData.setStockSymbol(stockCode);
					stockData = mapResponseToStockData(response, stockData);
					cacheService.put(stockCode, stockData);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		stockDataQueue.put(stockData);
		return stockData;
	}

	private StockData checkInCache(String stockCode) {
		return cacheService.get(stockCode);
	}

	private StockData mapResponseToStockData(HttpResponse response, StockData stockData) throws ParseException, JSONException, IOException {
		JSONObject jsonObject = new JSONObject(EntityUtils.toString(response.getEntity())).getJSONObject("query")
				.getJSONObject("results").getJSONObject("quote");
		stockData.setCurrentPrice(jsonObject.getString("Ask"));
		stockData.setYearTargetPrice(jsonObject.getString("OneyrTargetPrice"));
		stockData.setYearHigh(jsonObject.getString("YearHigh"));
		stockData.setYearLow(jsonObject.getString("YearLow"));
		return stockData;
	}

	private HttpResponse getAPIResponse(String stockCode) throws ClientProtocolException, IOException {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(Constant.API_URL);
		uriBuilder
		.replaceQueryParam("q",
				String.format(Constant.QUERY, stockCode))
		.replaceQueryParam("env", Constant.ENV)
		.replaceQueryParam("format", "json").replaceQueryParam("diagnostics", "true")
		.replaceQueryParam("callback", "");

		String URL = uriBuilder.build().encode().toUriString();

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(URL);
		return client.execute(request);
	}
}
